/**
 * @file ListeMEC.cpp
 * @author JB
 * @version 1 02/01/2014
 * @brief Composant de liste de messages en cours en m�moire dynamique et extensible
 */
 
#include <cassert> 
#include "ListeMEC.h"


void initialiser(ListeMEC& l, unsigned int capa, unsigned int pas) {
	assert ((capa>0) && (pas>0));
	initialiser(l.c, capa, pas);
	l.nb=0;
}


void detruire(ListeMEC& l) {
	detruire(l.c);
}

unsigned int longueur(const ListeMEC& l) {
	return l.nb;
}


MessageEnCours& lire(const ListeMEC& l, unsigned int pos) {
	assert(pos<l.nb);
	return lire(l.c, pos);
}

void ecrire(ListeMEC& l, unsigned int pos, const MessageEnCours& mec) {
	assert(pos<l.nb);
	ecrire(l.c, pos, mec);
}	

void inserer(ListeMEC& l, unsigned int pos, const MessageEnCours& mec) {
	assert(pos<=l.nb);
	for (unsigned int i=l.nb; i>pos; --i) {
		ecrire(l.c, i, lire(l, i-1));
	}
	ecrire(l.c, pos, mec);
	l.nb++;
}

void supprimer(ListeMEC& l, unsigned int pos) {
	assert(l.nb>0 && pos<l.nb);
	l.nb--;
	for (unsigned int i=pos; i<l.nb; ++i)
	   ecrire(l.c, i, lire(l,i+1));
}

MessageEnCours& newMessageEnCours(ListeMEC &l) {
	unsigned int c = longueur(l);

	{ // pour supprimer la var mec du contexte apres l'insertion
		MessageEnCours mec;

		inserer(l, c, mec);
	}

	MessageEnCours& mec2 = lire(l.c, c);

	initialiser(mec2);

	return mec2;
}

