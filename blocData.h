/**
 * @file BlocData.h
 * @author JB
 * @version 3 01/01/2015
 * @brief Composant de bloc de donnée
 */

#ifndef _BLOCDATA_
#define _BLOCDATA_


#include <iostream>


/** Type blocData
*/
struct blocData {
    enum { LENGTH_DATAS=20 + 1 }; // longueur de la 
    unsigned int noBloc;
    char data[LENGTH_DATAS];
};



/**
* @brief Lire une variable de type blocData
* à partir d’un flot en entrée is (de type ifstream) ouvert
* @param[out] bd : la variable de type blocData
* @param[in-out] is : le flot lut de type istream
* @pre is.good()
**/
void lire(blocData& bd, std::istream& is);


/**
* @brief Ecrire une variable id de type blocData
* dans un flot de sortie ouvert
* @param[in] bd : la variable de type blocData
* @param[out] is : le flot ecrit de type ofstream
* @pre !os.fail()
**/
void ecrire(const blocData& bd, std::ostream& os);


/**
 * @brief relation d'ordre entre 2 position
 * @param[in] b1 : le 1er bloc de donnees
 * @param[in] b2 : le 2eme bloc de donnees
 * @return true si b1 et b2 sont ordonnés, false sinon
 **/
bool enOrdre( const blocData& b1, const blocData& b2);



#endif // _BLOCDATA_
