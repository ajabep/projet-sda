#!/bin/bash


if [ "$1" == "" ]
then
	echo "[ERROR] Il n'y a pas de message de commit"
	echo "type '$0 -h' to display the help"
	exit
fi

if [ "$1" == "-h" ]
then
	echo "usage: $0 <commitMessage> [-t <tagName> [<tagMessage>]]"
	echo "   or: $0 <option>"
	echo ""
	echo "Options:"
	echo "    -h   display the help"
	echo "    -p   push only"
	exit
fi



if [ "$1" != "-p" ]
then
	
	echo ''
	echo 'Adding all new files'
	git add --all

	echo ''
	echo ''
	echo 'Commit all changes'
	git commit -m "$1"

	if [ "$2" == "-t" ]
	then
		echo ''
		echo ''
		echo 'Adding tags'
		tagName=$3
		tagMessage=$4
		# echo "tagName=$tagName"
		# echo "tagMessage=$tagMessage"
		if [ "$tagMessage" == "" ]
		then
			git tag -a "$tagName"
		else
			git tag -a "$tagName" -m "$tagMessage"
		fi
	fi

	echo ''
	echo ''
fi

echo 'synch'
git push origin master --tags
res=$?
if [ $res != 0 ]
then
	echo ''
	echo ''
	echo 'You are not up-to-date'
	echo 'You will be update'
	git pull origin master
	
	res=$?
	if [ $res == 0 ]
	then
		echo 'Now, you are up-to-date'
	else
		echo '[ERROR] An error occcured when the update'
		exit
	fi
	
	echo ''
	echo ''
	echo 're-synch'
	git push origin master --tags
	
	res=$?
	if [ $res == 0 ]
	then
		echo 'The synch is a success'
	else
		echo '[ERROR] The synch had an error'
	fi
else
	echo ''
	echo ''
	echo 'You are up-to-date'
	echo 'The synch is a success'
fi
