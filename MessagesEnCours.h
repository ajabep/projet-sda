/**
 * @file MessageEnCours.h
 * @author JB
 * @version 3 02/01/2015
 * @brief défini la structure de message en cours et ses opérations
 */

#ifndef _MESSAGE_EN_COURS_
#define _MESSAGE_EN_COURS_

#include "IdMessage.h"
#include "FilePrioriteBD.h"
#include "PaquetReseau.h"

struct MessageEnCours {
	IdMessage idMes;
	mutable FilePrioriteBD fileB;
	unsigned int LgMes; // longueur du message (noBloc du dernier blocData inséré)
	unsigned int nbPRecus; // nombre de PR reçu
	unsigned int LastPRecu; // NoPR du dernier PR reçu
};

/**
 * @brief initialiser un message en cours
 * @see detruire
 * @param[out] m : le message à initialiser
 */
void initialiser(MessageEnCours& m);

/**
 * @brief Désallouer la file d'un message en cours
 * @see initialiser
 * @param[out] m : le message
 */
void detruire(MessageEnCours& m);

/**
 * @brief Entrer un paquet réseau dans un message en cours
 * @param[in,out] m : le message en cours
 * @param[in] pr : le paquet réseau à entrer
 * @pre p.IdMes == m.idMes
 */
void entrer(MessageEnCours& m, const PaquetReseau& pr);

/**
 * @brief Ecrit la variable mec de type MessageEnCours
 * dans un flot de sortie ouvert
 * @param[in] mec : la variable de type MessageEnCours
 * @param[out] os : le flot ecrit de type ofstream
 * @pre !os.fail() && mec.LgMes == mec.nbPRecus
 **/
void ecrire(const MessageEnCours& mec, std::ostream& os);


#endif // _MESSAGE_EN_COURS_
