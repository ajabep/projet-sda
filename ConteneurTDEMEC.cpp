/**
 * @file ConteneurTDEMEC.cpp
 * @author JB
 * @version 1 01/01/2015
 * @brief Composant de conteneur de messages en cours de capacit� extensible
 */

#include <iostream>
#include <cassert>
using namespace std;

#include "ConteneurTDEMEC.h"



void initialiser(ConteneurTDEMEC& c, unsigned int capa, unsigned int p) {
	assert((capa>0) && (p>0));
	c.capacite = capa;
	c.pasExtension = p;
	// arr�t du programme en cas d'erreur d'allocation
	c.tab = new MessageEnCours[capa];
}


void detruire(ConteneurTDEMEC& c) {
	delete [] c.tab;
	c.tab = NULL;
}


MessageEnCours& lire(const ConteneurTDEMEC& c, unsigned int i) {
	assert(i < c.capacite);
	MessageEnCours& mec = c.tab[i];
	return mec;
}


void ecrire(ConteneurTDEMEC& c, unsigned int i, const MessageEnCours & mec) {
	if (i >= c.capacite) {
		/* Strat�gie de r�allocation proportionnelle au pas d'extension :
		 * initialisez la nouvelle taille du conteneur (newTaille)
		 * � i * c.pasExtension */
		unsigned int newTaille = (i + 1) * c.pasExtension;
		/* Allouez en m�moire dynamique un nouveau tableau (newT)
		 * � cette nouvelle taille*/
		MessageEnCours * newT = new MessageEnCours[newTaille];
		/* Recopiez les messages en cours d�j� stock�s dans le conteneur */
		for (unsigned int i = 0; i < c.capacite; ++i) {
			 newT[i] = c.tab[i];
			 // La d�salocation provoque un bug avec les r�f�rences (ChaineBD.courant)
			 // d'o� l'existace de la prochaine condition
			 if (c.tab[i].fileB.ch.courant == &(c.tab[i].fileB.ch.tete))
				 newT[i].fileB.ch.courant = &(newT[i].fileB.ch.tete);
		}
      	/* D�sallouez l'ancien tableau support du conteneur */
    	delete [] c.tab;
    	/* Actualiser la mise � jour du conteneur en m�moire dynamique
    	 * Faites pointer le tableau support du conteneur 
    	 * sur le nouveau tableau en m�moire dynamique */
    	c.tab = newT;
    	/* Actualisez la taille du conteneur */
    	c.capacite = newTaille;
	}
	/* Ecriture du message en cours (mec) � la position i dans le conteneur */
	c.tab[i] = mec;
}
