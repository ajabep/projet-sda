#ifndef _CONTENEUR_TDE_MEC_
#define _CONTENEUR_TDE_MEC_

/**
 * @file ConteneurTDEMEC.h
 * @author JB
 * @version 1 01/01/2015
 * @brief Composant d'un conteneur de messages en cours de capacit� extensible
 */

#include "MessagesEnCours.h"

/** @brief Conteneur d'items allou�s en m�moire dynamique
 *  de capacit� extensible suivant un pas d'extension
 */ 
struct ConteneurTDEMEC {
    unsigned int capacite; 	   // capacit� du conteneur (>0)
	unsigned int pasExtension; // pas d'extension du conteneur (>0)
	MessageEnCours* tab;	   // conteneur allou� en m�moire dynamique
};

/**
 * @brief Initialise un conteneur d'items
 * Allocation en m�moire dynamique du conteneur d'items
 * de capacit� (capa) extensible par pas d'extension (p)
 * @see detruire, pour sa d�sallocation en fin d'utilisation 
 * @param[out] c : le conteneur d'items
 * @param [in] capa : capacit� du conteneur
 * @param [in] p : pas d'extension de capacit�
 * @pre capa>0 et p>0
 */
void initialiser(ConteneurTDEMEC& c, unsigned int capa, unsigned int p);

/**
 * @brief D�salloue un conteneur d'items en m�moire dynamique
 * @see initialiser, le conteneur d'items a d�j� �t� allou� 
 * @param[in,out] c : le conteneur d'items
 */
void detruire(ConteneurTDEMEC& c); 

/**
 * @brief Lecture d'un message en cours d'un conteneur de message en cours
 * @param[in] c : le conteneur de message en cours
 * @param[in] i : la position du message en cours dans le conteneur
 * @return r�f�rence du message en cours � la position i
 *   Ce parametre est en retour car le parametre de sortie ne pointait
 *   pas sur le message en cours
 * @pre i < c.capacite
 */
MessageEnCours& lire(const ConteneurTDEMEC& c, unsigned int i);

/**
 * @brief Ecrire un message en cours dans un conteneur de messages en cours
 * @param[in,out] c : le conteneur de messages en cours
 * @param[in] i : la position o� ajouter/modifier le message en cours
 * @param[in] mec : le message en cours � �crire
 */
void ecrire(ConteneurTDEMEC& c, unsigned int i, const MessageEnCours & mec);

#endif /*_CONTENEUR_TDE_MEC_*/
