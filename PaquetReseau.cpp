/**
 * @file PaquetReseau.cpp
 * @author JB
 * @brief fonctions associ�es � la structure PaquetReseau
 * @version 2 02/01/2015
 **/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include "PaquetReseau.h"


void initialiser(PaquetReseau& pr) {
	pr.noPR = 0;
	pr.finMess = false;
}

void lire(PaquetReseau& pr, std::istream& is) {
	assert(is.good());

	is >> pr.noPR;
	lire(pr.IdMes, is);
	is >> pr.finMess;

	lire(pr.blocD, is);
}

/**
* @brief Ecrire la variable id de type PaquetReseau
* dans un flot de sortie ouvert
* @param[in] id : la variable de type PaquetReseau
* @param[out] is : le flot ecrit de type ofstream
* @pre !os.fail()
**/
void ecrire(const PaquetReseau& pr, std::ostream& os) {

	os << pr.noPR << " ";
	ecrire(pr.IdMes, os);
	
	os << " " << pr.finMess << std::endl;

	ecrire(pr.blocD, os);

	os << std::endl << std::flush;
}




