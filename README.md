Projet SDA
=======

Note dite lors du cours
-----------------------


* fin des questions lundi 15/12


### Remise des dossiers

* Remise des dossiers papier au secretaria le 12 janvier
* Remise des dossiers électrnique dans le puits jusqu'au 12 janvier à 23h59




messages affiché ainsi : 
```
NuméroPaquetReseau Expediteur GroupesConcernés Date Heure FinDeMessage NuméroDeBlockDeDonnées
Données formatés sur t caractères
```




### Tri mémoire des messages

ce fait avec une file à priorité. On devra faire la priorité dans la file


Convention de nomage
--------------------

### Variables

Les nom des variables commencent avec une minuscules. Lors de la séparation pour un autre mot, on ne met pas l'espace et la première lettre du second mot commence par une majuscule.

```
Exemples :
int maSuperVairable;
float ceNomEstGenial;
```
### Fonctions

Les noms respectent la même norme que les variables.


### Constantes

Les noms des constantes sont en majuscules. Pour séparer des mots, on utilisera des tirets bas.
 
```
Exemples :
#define MA_SUPER_CONTANTE
const CE_NOM_EST_ABSOLUENT_AWESOME
```