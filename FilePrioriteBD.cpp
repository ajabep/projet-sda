/**
 * @file FilePrioriteBD.cpp
 * @author JB
 * @version 1 - 31/12/2014
 * @brief Composant de file d'items stock�e dans une cha�ne � simple cha�nage
 */
 
#include <cassert>
#include "FilePrioriteBD.h"


void initialiser(FilePrioriteBD& f) {
    initialiser(f.ch);
	f.indPremier = 0;
	f.indProchain = 0;
	f.nb = 0;
}

void detruire(FilePrioriteBD& f) {
	 detruire(f.ch);
}

bool estVide(const FilePrioriteBD& f) {
	return (estVide(f.ch));
}

blocData tete(const FilePrioriteBD& f) {
    assert(!estVide(f));
    debut(f.ch);
    return lire(f.ch);
}

void entrer(FilePrioriteBD& f, const blocData& bd) {
    fin(f.ch);
	inserer(f.ch, bd);
}

void sortir(FilePrioriteBD& f) {
	assert(!estVide(f));
	debut(f.ch);
	supprimer(f.ch);
}

void ecrire(const FilePrioriteBD& f, std::ostream& os) {
	assert(!os.fail() && !estVide(f));
	debut(f.ch);
	while (!estFin(f.ch)) {
		ecrire(lire(f.ch), os);
		suivant(f.ch);
	}
	os << std::endl;
}
