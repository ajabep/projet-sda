/**
 * @file testChaine_Positions.cpp
 * Projet sem06-tp-Cpp1
 * @author l'�quipe p�dagogique 
 * @version 1 26/01/06
 * @brief Test d'un conteneur de positions de type cha�ne (� simple cha�nage)
 * Structures de donn�es et algorithmes - DUT1 Paris 5
 */

#include <iostream>
using namespace std;
 
#include "Chaine.h"

/* Test d'une cha�ne de positions */ 
int main(int argc, char* argv[]) {

    Chaine cPositions; // D�claration de la cha�ne de positions
    
	// Initialisation de cPositions � la cha�ne vide ..........................
    initialiser(cPositions);
  
    Position p;
  
    cout << "Test d'une cha�ne de positions" << endl;
    /* Ajout de positions dans la chaine
     * jusqu'� la saisie d'une position d'absisse 0 (non ajout�e) */	
    cout << "Saisir des positions jusqu'� la saisie de l'origine [0,0]\n";  
    cout << "Les positions (� l'exception de celle de l'origine)\n";
    cout << "seront ajout�es en fin de cha�ne" << endl;
    bool estOrigine;
    do {
        p = saisir();
        estOrigine = (p.abscisse == 0) && (p.ordonnee == 0);
        // Codez l'ajout de p (diff�rent de [0,0]) en fin de cha�ne ...........
        if (!estOrigine) {
         	fin(cPositions); 
        	inserer(cPositions, p);
        } 
    } while (!estOrigine);
  
    // Affichage des �l�ments de la cha�ne
    cout << "Etat de la cha�ne en fin de saisie :" << endl;
    // Codez l'affichage de la cha�ne .........................................
    debut(cPositions); // d�placement du maillon courant en d�but de cha�ne
    while (!estFin(cPositions)) {
        afficher(lire(cPositions));
        suivant(cPositions);
    }
    cout << endl;
  
    // Insertion d'un nouvel �l�ment [7, 8] en fin de cha�ne
    cout << "Insertion d'un nouvel �l�ment [7, 8] en fin de cha�ne" << endl;
    p.abscisse = 7; p.ordonnee = 8;
	// Codez l'insertion de p .................................................
    fin(cPositions);
    inserer(cPositions, p);
  
    // Suppression de l'�l�ment en d�but de cha�ne
    cout << "Suppression de l'�l�ment en d�but de cha�ne" << endl;
	// Codez la suppression ...................................................    
    debut(cPositions);
    supprimer(cPositions);

    // Affichage de l'�tat de la cha�ne
  	cout << "Etat de la cha�ne :" << endl;
    // Recopiez l'affichage de la cha�ne ......................................
  	debut(cPositions);
    while (!estFin(cPositions)) {
        afficher(lire(cPositions));
        suivant(cPositions);
    }
    cout << endl;
  
    // Codez la d�sallocation de la cha�ne ....................................
    detruire(cPositions);
    
    return 0;  
}
