/**
 * @file testListePositions.cpp
 * Projet sem06-tp-Cpp3
 * @author l'�quipe p�dagogique 
 * @version 1 26/01/06
 * @brief Test d'une liste de positions dont le conteneur est 
 * une cha�ne � simple cha�nage
 * Structures de donn�es et algorithmes - DUT1 Paris 5
 */

#include <iostream>
using namespace std;
 
#include "Liste.h"

/* Test d'une liste de positions */ 
int main(int argc, char* argv[]) {

	Liste lPositions; 	// D�claration de la liste de positions
	Position p;

	// Initialisation de lPositions � la liste vide
	initialiser(lPositions);	

	cout << "Test d'une liste de positions" << endl;

	/* Ajout de positions en d�but de liste
	 * jusqu'� la saisie d'une position d'absisse 0 (non ajout�e) */	
	cout << "Saisir des positions jusqu'� la saisie de l'origine [0,0]\n";  
	cout << "Les positions (� l'exception de celle de l'origine)\n";
	cout << "seront ajout�es en d�but de liste" << endl;
	bool estOrigine;
	do {
		p = saisir();
		estOrigine = (p.abscisse == 0) && (p.ordonnee == 0);
		if (!estOrigine) inserer(lPositions, longueur(lPositions), p); 
	} while (!estOrigine);

	// Affichage des �l�ments de la liste

	cout << "La liste contient " << longueur(lPositions) <<" �l�ment(s) :\n";
	for (unsigned int i = 0; i < longueur(lPositions); ++i) 
		afficher(lire(lPositions, i));
	cout << endl;

    // Insertion de deux nouveaux �l�ments [7, 8] et [8, 9] en fin de liste
	cout << "Insertion de l'�l�ment [7, 8] en fin de liste" << endl;
	p.abscisse = 7; p.ordonnee = 8;
	inserer(lPositions, longueur(lPositions), p);
	cout << "Insertion de l'�l�ment [8, 9] en fin de liste" << endl;
	p.abscisse = 8; p.ordonnee = 9;
	inserer(lPositions, longueur(lPositions), p);
    
    // Suppression du deuxi�me �l�ment de liste */
    cout << "Suppression du deuxi�me �l�ment de liste" << endl;
	supprimer(lPositions, 1);

	// Affichage de la longueur de la liste
	cout << "Longueur de la liste : ";
	cout << longueur(lPositions) << endl;

	// Affichage des �l�ments de la liste
	cout << "Etat de la liste :" << endl;
	for (unsigned int i = 0; i < longueur(lPositions); ++i) 
		afficher(lire(lPositions, i));
	cout << endl;

	// D�sallouez la liste
	detruire(lPositions);
	
	return 0;    
}
