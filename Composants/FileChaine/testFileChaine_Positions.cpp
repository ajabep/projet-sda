/**
 * @file testFileChaine_Positions.cpp
 * Projet sem06-tp-Cpp2
 * @author l'�quipe p�dagogique 
 * @version 1 26/01/06
 * @brief Test d'une file de positions dont le conteneur est
 * une cha�ne � simple cha�nage
 * Structures de donn�es et algorithmes - DUT1 Paris 5
 */

#include <iostream>
using namespace std;
 
#include "File.h"

/* Test d'une file de positions */ 
int main(int argc, char* argv[]) {

    File fPositions; 	// D�claration de la file de positions
    
	// Initialisation de fPositions � la cha�ne vide
    initialiser(fPositions);
    
    Position p;
    
    cout << "Test d'une file de positions" << endl;
    
    /* Ajout de positions dans la file
     * jusqu'� la saisie de l'origine (non ajout�e en file) */	
    cout << "Saisir des positions jusqu'� la saisie de l'origine [0,0]\n";  
    cout << "Les positions (� l'exception de celle de l'origine)\n";
    cout << "seront ajout�es � la file" << endl;
    bool estOrigine;
    do {
        p = saisir();
        estOrigine = (p.abscisse == 0) && (p.ordonnee == 0);
		if (!estOrigine) entrer(fPositions, p); 
    } while (!estOrigine);
    
    // Sortie de file d'un el�ment si la file n'est pas vide
    cout << "Sortie de file d'un �l�ment si la file n'est pas vide" << endl;
	if (!estVide(fPositions)) sortir(fPositions);
    
    // Entr�e d'un nouvel �l�ment [7, 8] dans la file
    cout << "Entr�e d'un nouvel �l�ment [7, 8] dans la file" << endl;
    p.abscisse = 7; p.ordonnee = 8;
 	entrer(fPositions, p);
    
    // Affichage de la file
    cout << "Etat de la file :" << endl;
    while (!estVide(fPositions)) {
        afficher(tete(fPositions));
        sortir(fPositions);
    }
    
    // D�sallocation de la file
    detruire(fPositions);
    
    return 0;
}
