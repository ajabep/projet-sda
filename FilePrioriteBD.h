#ifndef _FILE_
#define _FILE_

/**
 * @file FilePriorite.h
 * @author JB
 * @version 2 01/01/2015
 * @brief Composant de file de bloc de donn�e stock�e dans une cha�ne � simple cha�nage
 */

#include <iostream>
#include "ChaineBD.h"

/**
 *  File g�r�e dans une cha�ne
 */
struct FilePrioriteBD {
	mutable ChaineBD ch;	 // cha�ne m�morisant les �l�ments de file
	unsigned int indPremier; // index de la t�te de file dans tab
	unsigned int indProchain;// index du prochain �l�ment entr� en file
	unsigned int nb;		 // nombre d'�l�ments dans la file
};

/**
 * @brief initialiser une file vide
 * la file est allou�e en m�moire dynamique
 * @see detruire, elle est � d�sallouer en fin d�utilisation
 * @param[out] f : la file � initialiser
 */
void initialiser(FilePrioriteBD& f);

/**
 * @brief D�sallouer une file
 * @see initialiser, la file a d�j� �t� allou�e en m�moire dynamique
 * @param[out] f : la file 
 */
void detruire(FilePrioriteBD& f);


bool estVide(const FilePrioriteBD& f);


blocData tete(const FilePrioriteBD& f);

/**
 * @brief Entrer un bloc de donn�e dans la file
 * @param[in,out] f : la file
 * @param[in] bd : le bloc de donn�e � entrer
 */
void entrer(FilePrioriteBD& f, const blocData& bd);

/**
 * @brief Sortir le bloc de donn�e t�te de file
 * @param[in,out] f : la file
 * @pre f n�est pas vide
 */
void sortir(FilePrioriteBD& f);

/**
 * @brief Ecrit la variable f de type FilePrioriteBD
 * dans un flot de sortie ouvert
 * @param[in] f : la variable de type FilePrioriteBD
 * @param[out] os : le flot ecrit de type ofstream
 * @pre os est pret � �crire et f n'est pas vide
 */
void ecrire(const FilePrioriteBD& f, std::ostream& os);

#endif
