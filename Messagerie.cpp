/**
 * @file Messagerie.cpp
 * @author JB
 * @version 1 02/01/2015
 * @brief fonctions associées à la structure de messagerie
 */

#include <iostream>
#include <iomanip>
#include "Messagerie.h"


void initialiser(Messagerie& m) {
	initialiser(m.listeM, 1, Messagerie::PAS_EXTENTION_LISTE);
}

void detruire(Messagerie& m) {
	detruire(m.listeM);
}

void recevoirPaquetReseau(std::istream& is, PaquetReseau& p) {
	// pas à faire au sprint 2
}

void traiterPaquetReseau(Messagerie& m, const PaquetReseau& p) {
	unsigned int i = 0,
		         c = longueur(m.listeM);

	for (; i < c; ++i) {
		if ( estEgal( p.IdMes, lire(m.listeM, i).idMes ) )
			break;
	}

	if (i == c) {
		// nouveau message
		MessageEnCours &mec = newMessageEnCours(m.listeM);

		mec.idMes = p.IdMes;
		
		//entrer(mec, p);
	}
	/* pas au sprint 2 ...
	else if (p.noPR >= lire(m.listeM, i).LastPRecu + 10) // pour simmuler la perte d'un paquet réseau
		supprimer(m.listeM, i);
	*/
	else {
		MessageEnCours &mec = lire(m.listeM, i);

		// continuité d'un message en cours
		//entrer(mec, p);
	}
}
