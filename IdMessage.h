/**
 * @file IdMessage.h
 * @author JB
 * @version 3 01/01/2015
 * @brief défini la structure d'identification du message et ses opérations
 */


#ifndef _IDMESSAGE_
#define _IDMESSAGE_


#include <iostream>


/**
 * @brief Type IdMessage
 **/
struct IdMessage {
	enum {
		LENGTH_EXP = 120 + 1,
		LENGTH_DEST = 120 + 1,
		LENGTH_DATE = 8 + 1,
		LENGTH_HOUR = 8 + 1
	};
	char exp[LENGTH_EXP],
		 dest[LENGTH_DEST],
		 d[LENGTH_DATE],
		 h[LENGTH_HOUR];
};

/**
 * @brief Lit un identificateur message
 * à partir d’un flot en entrée (de type ifstream) ouvert
 * @param[out] id : la variable de type IdMessage
 * @param[in-out] is : le flot lut de type istream
 * @pre is.good()
 **/
void lire(IdMessage& id, std::istream& is);

/**
 * @brief Ecrit un identificateur message
 * dans un flot de sortie ouvert
 * @param[in] id : la variable de type IdMessage
 * @param[out] is : le flot ecrit de type ofstream
 * @pre !os.fail()
 **/
void ecrire(const IdMessage& id, std::ostream& os);

/**
 * @brief vérifie l'équivalence entre deux conteneur de type IdMessage
 * @param [in] id1 : conteneur à comparer 1
 * @param [in] id2 : conteneur à comparer 2
 * @return true si id1 et id2 sont egaux, false sinon
 **/
bool estEgal(const IdMessage& id1, const IdMessage& id2);



#endif // _IDMESSAGE_
