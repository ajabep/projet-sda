/**
 * @file Messagerie.h
 * @author JB
 * @version 3 02/01/2015
 * @brief défini la structure de messagerie et ses opérations
 */

#ifndef _MESSAGERIE_
#define _MESSAGERIE_

#include <iostream>
#include <iomanip>
#include "ListeMEC.h"

struct Messagerie {
	ListeMEC listeM; // liste des messages
	enum {
		PAS_EXTENTION_LISTE = 1
	};
};

/**
 * @brief alloue une variable de messagerie
 * @see detruire pour désallouer
 * @param[out] la messagerie à allouer
 */
void initialiser(Messagerie& m);

/**
 * @brief désalloue une variable de messagerie
 * @see initialiser pour allouer
 * @param[out] la messagerie à désallouer
 */
void detruire(Messagerie& m);

/**
 * @brief Reception d'un paquet-réseau
 * @param[in] is flux d'entré pour la lecture du paquet
 * @param[out] p le paquet réseau dans lequel on lit le flux d'entré
 **/
void recevoirPaquetReseau(std::istream& is, PaquetReseau& p);

/**
* @brief Organiser un paquet-réseau dans la messagerie
* @param[in-out] m la messagerie
* @param[in] p le paquet réseau à insérer
*/
void traiterPaquetReseau(Messagerie& m, const PaquetReseau& p);

#endif // _MESSAGERIE_
