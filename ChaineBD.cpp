/**
 * @file ChaineBD.cpp
 * @author JB
 * @version 1 01/01/2015
 * @brief fonctions associ�es � la structure ChaineBD
 **/

#include <iostream>
#include <cassert>
using namespace std;

#include "ChaineBD.h"


void initialiser(ChaineBD& c) {
    c.tete = NULL;
	c.courant = &c.tete;
}


void detruire(ChaineBD& c) {
    debut(c);
    while (!estVide(c)) 
        supprimer(c);
}


bool estVide(const ChaineBD& c) {
    return c.tete == NULL;
}


void inserer(ChaineBD& c, const blocData& elem) {
    MaillonBD* m = new MaillonBD;
    m->elem = elem;
    m->suiv = *(c.courant);
    *(c.courant) = m;
}


void supprimer(ChaineBD& c) {
    assert(!estFin(c));
    MaillonBD* m = *(c.courant);
    *(c.courant) = (*(c.courant))->suiv;
    delete m;
}


void ecrire(ChaineBD& c, const blocData& elem) {
    assert(!estFin(c));
    (*(c.courant))->elem = elem;
}


blocData lire(const ChaineBD& c) {
    assert(!estFin(c));
    return (*(c.courant))->elem;
}


void debut(ChaineBD& c) {
    c.courant = &(c.tete);
}


bool estDebut(const ChaineBD& c) {
    return c.courant == &(c.tete);
}


void fin(ChaineBD& c) {
	while (!estFin(c)) {
		suivant(c);
	}
}


bool estFin(const ChaineBD& c) {
	return *(c.courant) == NULL;
}


void suivant(ChaineBD& c) {
	assert(!estFin(c));
	c.courant = &((*(c.courant))->suiv);
}
