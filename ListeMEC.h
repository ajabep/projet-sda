#ifndef _LISTE_MEC_
#define _LISTE_MEC_

/**
 * @file ListeMEC.h
 * @author JB
 * @version 1 02/01/2014
 * @brief Composant de liste de messages en cours en m�moire dynamique et extensible
 */

#include "ConteneurTDEMEC.h"

struct ListeMEC {
	ConteneurTDEMEC c; // tableau m�morisant les �l�ments de la liste
    unsigned int nb;   // nombre d'�l�ments stock�s dans la liste
};

/**
 * @brief Initialiser une liste vide
 * la liste est allou�e en m�moire dynamique
 * @see detruire, la liste est � d�sallouer en fin d�utilisation
 * @param[out] l : la liste � initialiser
 * @param[in] capa : capacit� de la liste
 * @param[in] pas : pas d�extension de la liste
 * @pre capa>0 et pas>0
 */
void initialiser(ListeMEC& l, unsigned int capa, unsigned int pas);

/**
 * @brief D�sallouer une liste
 * @see initialiser, la liste a d�j� �t� allou�e en m�moire dynamique
 * @param[out] l : la liste
 */
void detruire(ListeMEC& l);

/**
 * @brief Longueur de liste
 * @param[in] l : la liste
 * @return la longueur de la liste
 */
unsigned int longueur(const ListeMEC& l);

/**
 * @brief Lire un �l�ment de liste
 * @param[in] l : la liste
 * @param[in] pos : position de l'�l�ment � lire
 * @return : le message en cours lu en position pos
 *   Ce parametre est en retour car le parametre de sortie ne pointait
 *   pas sur le message en cours
 * @pre 0<=pos<longueur(l)
 */
MessageEnCours& lire(const ListeMEC& l, unsigned int pos);

/**
 * @brief Ecrire un message en cours dans une liste de messages en cours
 * @param[in,out] l : la liste
 * @param[in] pos : position du message en cours � �crire
 * @param[in] mec : le message en cours
 * @pre 0<=pos<longueur(l)
 */
void ecrire(ListeMEC& l, unsigned int pos, const MessageEnCours& mec);

/**
 * @brief Ins�rer un �l�ment dans une liste
 * @param[in,out] l : la liste
 * @param[in] pos : la position � laquelle l'�l�ment est ins�r�
 * @param[in] it : l'�l�ment ins�r�
 * @pre 0<=pos<=longueur(l)
 * l�insertion est faite avant la position pos
 */
void inserer(ListeMEC& l, unsigned int pos, const MessageEnCours& mec);

/**
 * @brief Supprimer un �l�ment dans une liste
 * @param[in,out] l : la liste
 * @param[in] pos : la position de l'�l�ment � supprimer
 * @pre longueur(l)>0 et 0<=pos<longueur(l)
 */
void supprimer(ListeMEC& l, unsigned int pos);

/**
 * @brief ajoute et initialise un nouveau message en cours
 * nous avons cr�� cette fontion pour r�soudre le probleme de reference
 * que causait le fait d'ins�rer un message apres l'avoir instancifier
 * @param [in-out] l la liste
 * @return le message en cours
 *   Ce parametre est en retour car le parametre de sortie ne pointait
 *   pas sur le message en cours
 **/
MessageEnCours& newMessageEnCours(ListeMEC &l);



#endif // _LISTE_MEC_
