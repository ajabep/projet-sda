/**
 * @file IdMessage.cpp
 * @author JB
 * @version 2 01/01/2015
 * @brief fonctions associ�es � la structure IdMessage
 **/


#include "IdMessage.h"
#include <iomanip>
#include <cassert>




void lire(IdMessage& id, std::istream& is) {
	assert(is.good());

	is >> std::setw(IdMessage::LENGTH_EXP) >> id.exp;
	is >> std::setw(IdMessage::LENGTH_DEST) >> id.dest;
	is >> std::setw(IdMessage::LENGTH_DATE) >> id.d;
	is >> std::setw(IdMessage::LENGTH_HOUR) >> id.h;
}


void ecrire(const IdMessage& id, std::ostream& os) {
	assert(!os.fail());
	os << id.exp << " " << id.dest << " " << id.d << " " << id.h;
}


bool estEgal(const IdMessage& id1, const IdMessage& id2) {
	return !strcmp(id1.d, id2.d) && !strcmp(id1.h, id2.h) && !strcmp(id1.exp, id2.exp) && !strcmp(id1.dest, id2.dest);
}





