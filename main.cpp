/**
 * @file main.cpp
 * @author JB
 * @brief controleur principal
 * @version 1 20/12/2014
 **/

#include <iostream>
#include <fstream>

#include "IdMessage.h"
#include "PaquetReseau.h"
#include "Messagerie.h"

using namespace std;

int main() {
	Messagerie m;
	PaquetReseau pr;

	initialiser(m);
	initialiser(pr);
	
	while (!cin.eof()) {
		lire(pr, cin);
		traiterPaquetReseau(m, pr);
		cin >> ws;
	}
	
	for (unsigned int i = 0, c = longueur(m.listeM); i < c; ++i) {
		ecrire(lire(m.listeM, i), cout);
	}

	cout << endl;

	detruire(m);

	return 0;
}
