/**
 * @file MessageEnCours.cpp
 * @author JB
 * @version 1 02/01/2015
 * @brief fonctions associées à la structure de message en cours
 */

#include <cassert>
#include "MessagesEnCours.h"



void initialiser(MessageEnCours& m) {
	initialiser(m.fileB);
	m.LgMes = NULL;
	m.nbPRecus = NULL;
	m.LastPRecu = NULL;
}

void detruire(MessageEnCours& m) {
	detruire(m.fileB);
	m.LgMes = NULL;
	m.nbPRecus = NULL;
	m.LastPRecu = NULL;
}

void entrer(MessageEnCours& m, const PaquetReseau& pr) {
	assert( estEgal(pr.IdMes, m.idMes) );

	entrer(m.fileB, pr.blocD);
	++m.nbPRecus;
	m.LastPRecu = pr.noPR;

	if (pr.finMess)
		m.LgMes = pr.blocD.noBloc;

}

void ecrire(const MessageEnCours& mec, std::ostream& os) {
	assert(!os.fail() && mec.LgMes == mec.nbPRecus);

	ecrire(mec.idMes, os);

	os << std::endl;

	//ecrire(mec.fileB, os);

	//os << std::endl;
}
