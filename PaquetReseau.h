/**
 * @file PaquetReseau.h
 * @author JB
 * @version 3 02/01/2015
 * @brief défini la structure de paquet-réseau et ses opérations
 */

#ifndef _PAQUET_RESEAU_
#define _PAQUET_RESEAU_

#include "IdMessage.h"
#include "BlocData.h"

struct PaquetReseau {
	unsigned int noPR;
	IdMessage IdMes;
	blocData blocD;
	bool finMess;
};

/**
 * @brief Initialise une structure de type PaquetReseau
 * @param [out] structure à initialiser
 **/
void initialiser(PaquetReseau& pr);

/**
 * @brief Lire une variable PaquetReseau
 * à partir d’un flot en entrée is (de type ifstream) ouvert
 * @param[out] pr : la variable de type PaquetReseau
 * @param[in-out] is : le flot lut de type istream
 * @pre is.good()
 **/
void lire(PaquetReseau& pr, std::istream& is);

/**
 * @brief Ecrire une variable PaquetReseau
 * dans un flot de sortie ouvert
 * @param[in] pr : la variable de type PaquetReseau
 * @param[out] is : le flot ecrit de type ofstream
 * @pre !os.fail()
 **/
void ecrire(const PaquetReseau& pr, std::ostream& os);



#endif // _PAQUET_RESEAU_
