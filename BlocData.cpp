/**
* @file BlocData.cpp
* @author JB
* @version 2 01/01/2015
* @brief fonctions associ�es � la structure blocData
**/


#include <iomanip>
#include <cassert>
#include "BlocData.h"



void lire(blocData& bd, std::istream& is) {
	assert(is.good());

	is >> bd.noBloc;

	is.ignore();// pour le \n

	is.read(bd.data, blocData::LENGTH_DATAS - 1);
	bd.data[blocData::LENGTH_DATAS - 1] = '\0'; // pour mettre le \0 � la fin de la chaine de caract�res
}


void ecrire(const blocData& bd, std::ostream& os) {
	assert( !os.fail() );
	os << bd.data;
}


bool enOrdre(const blocData& b1, const blocData& b2) {
	return b1.noBloc > b2.noBloc;
}



